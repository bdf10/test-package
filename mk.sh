#!/bin/sh

CHROOT=$HOME/.cache/chroot
mkdir -p $CHROOT
mkarchroot $CHROOT/root base-devel
arch-nspawn $CHROOT/root pacman -Syu
makechrootpkg -cr $CHROOT
